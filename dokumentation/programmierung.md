Die Programmierung von Luna LMS
===============================

## Sprache

Wir dokumentieren Luna LMS in deutscher Sprache.

Ausnahme: Kommentare im Programm-Code schreiben wir auf Englisch.


## Programmier-Stil

In Python verwenden wir Tabulatoren zum Einrücken.


## Daten-Speicherung

### Leitgedanken

Wir wollen so viel wie möglich im Dateisystem abbilden.

Wir wollen Verwirrung von Laien minimieren.

### Lern-Baustein

Ein Lern-Baustein ist das kleinstes Element in Luna.

- Jeder Lern-Baustein ist entweder eine Datei oder ein Ordner.
    - Diese Dateien und Ordner sind **nicht** die Mediathek.
    - Dateien enthalten die zulässigen Datentypen
        - insbesondere: .html (für HTML-Schnipsel, keine kompletten Dateien), .png, .jpg, .mp3 etc.
        - Dateien dürfen beliebige, im Betriebssystem zulässige Namen haben
            - Das ist ein Zugeständnis an Nicht-Programmierer*innen, die verwirrt wären, wenn ihre "Lufballon.JPEG" plötzlich "2022-04-02-img445-67.jpg" heißt.
    - Ordner entstehen, wenn ein Lern-Baustein zwingend aus mehreren Dateien besteht
        - Zum Beispiel: HTML mit extra JavaScript
        - Zum Beispiel: HTML mit Bildern an ganz bestimmter Position
        - Ordner sollen die Ausnahme sein
    - Beispiel: _Einleitung.mp3_, _Begrueßung.JPG_, _Einleitung_ (Ordner mit _Einleitung/Inhalt.html_, _Einleitung/Hintergrund.png_)
- Zu jeder Date mit Bezug zu Lern-Bausteinen muss eine Datei mit Meta-Informationen existieren
    - Die Datei trägt den gleichen Namen wie die Lern-Baustein-Datei oder der Ordner, inklusive eventueller Dateiendung, und fügt eine neue Endung an
        - Bei Lern-Baustein-Ordnern liegt die Meta-Datei im Ordner
    - Die Endung ist .meta
    - Die Meta-Datei ist eine Text-Datei, die JSON enhält
    - Die Meta-Daten werde nach dem [Dublin Core-Standard](https://en.wikipedia.org/wiki/Dublin_Core) notiert.
        - Offizielle aktuelle Spezifikation: https://www.dublincore.org/specifications/dublin-core/dcmi-terms/
    - Die Metadaten müssen mindestens die Werte "identifier", "format" und "type" beschreiben.
        - Der identifier ist eine UUID
        - Das format ist der MIME-Type der Datei
        - Der type ist "Lern-Baustein", "Lern-Einheit", "Lern-Pfad", "Kurs-Einheit", "Datei"
        - Beispiel: _Luftballon.JPEG.meta_ -> _{"identifier": "fbea4806-5486-480b-8772-cb93f96bc97d", "format": "image/jpeg", "type": "Lern-Baustein"}_
    - Später können wir dort sehr leicht z. B. Copyright oder Lizenz pro Lern-Baustein erfassen
        - Auch Ersteller, Bearbeiterin, ...
    - Probleme ergeben sich aus der Konsistenz der Meta-Dateien


### Lern-Einheit

Besteht aus einem oder mehreren Lern-Bausteinen.

- Die Lern-Einheit ist ein Ordner, der die zugehörigen Lern-Bausteine enthält
- Der Name des Ordners ist eine UUID, die gleichzeitig "identifier" der Lern-Einheit ist
- Im Ordner der Lern-Einheit liegt wieder eine Meta-Datei für den Ordner
- Sie muss mindestens den identifier, den type und einen title enthalten
- Beispiel: _Ordner ca327653-11db-4a06-9ef8-ab18e9072527_ , darin Meta-Datei _ca327653-11db-4a06-9ef8-ab18e9072527.meta_ -> _{"identifier": "ca327653-11db-4a06-9ef8-ab18e9072527", "type": "Lern-Einheit", "title": "Einleitung zur Kurs-Einheit"}_
- Alle Lerneinheiten liegen in einem Ordner "Lern-Einheiten"


### Lern-Pfad

Eine gerichtete Abfolge aus Lern-Bausteinen in Lern-Einheiten; die angesteuerten Lern-Bausteine kann die Lernerin selbst live bestimmen.

- Lern-Pfade ergeben sich insbesondere zur Laufzeit
- Lern-Pfade können vorgeneriert werden, z. B. "Pfad aus allen Audio-Bausteinen"
- später legen wir die im Ordner "Lern-Pfade" ab
- werden vorerst nicht implementiert


### Kurs-Einheit

Alle Lern-Pfade, Lern-Einheiten und Lern-Bausteine, die eine abgeschlossene Lern-Erfahrung ergeben.

- Im Dateisystem entspricht der Kurs-Einheit dem Wurzel-Ordner mit allen hierarchisch geordneten Dateien
- Der Wurzel-Ordner enthält eine Datei mit dem Namen des Kurseinheits-Ordners und der Endung .json
    - Sie beschreibt die Kurs-Einheit mit allen notwendigen Meta-Daten
    - Aber auch die geordnete Reihenfolge der Lern-Einheiten

Zum Beispiel:

- Meine_Kurseinheit
    - Meine_Kurseinheit.json
    - Lern-Einheiten
        - ca327653-11db-4a06-9ef8-ab18e9072527
            - ca327653-11db-4a06-9ef8-ab18e9072527.meta -> type: Lern-Einheit
            - Einleitung.mp3.meta -> type: Lern-Baustein
            - Einleitung.mp3
            - Begrueßung.JPG.meta -> type: Lern-Baustein
            - Begrueßung.JPG
            - Einleitung
                - Einleitung.meta -> type: Lern-Baustein
                - Inhalt.html
                - Inhalt.html.meta -> type: Datei
                - Hintergrund.png
                - Hintergrund.png.meta -> type: Datei
        - 20574716-6daa-42e4-a8ce-12a006b016b7
            - ...
    - Lern-Pfade
        - ...

Meine_Kurseinheit.json:

	{
	    "identifier": "cd4a73ec-dbc5-4c1f-947a-e2b0b7e16e67",
	    "type": "Kurs-Einheit",
	    "title": "Meine Kurseinheit",
	    "Lern-Einheiten": [
	                          "20574716-6daa-42e4-a8ce-12a006b016b7",
	                          "ca327653-11db-4a06-9ef8-ab18e9072527"
	                      ],
	    "description": "...",
	    "language": "...",
	    ...
	}
